# 👋 Howdy there, I'm Jeremy Novak


I'm currently employed at [Microsoft](https://azure.microsoft.com) as an [Azure App Services](https://azure.microsoft.com/en-us/services/app-service/) Engineer. I love to work with Open Source Software, Web Applications, Containers and the Cloud. 

<a href="https://linkedin.com/in/jgnovak" target="_blank" title="Linkedin"><img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" /></a>



## :rocket: About:

I really enjoy working with these languages, frameworks and tools. 

<a href="https://python.org" target="_blank" title="Python">Python</a>  
<a href="https://go.dev" target="_blank" title="Go">Go</a>  
<a href="https://www.ruby-lang.org" target="_blank" title="Ruby">Ruby</a>  
<a href="https://rubyonrails.org/" target="_blank" title="Ruby on Rails">Ruby on Rails</a>  
<a href="https://tailwindcss.com" target="_blank" title="Tailwind CSS">Tailwind CSS</a>  
<a href="https://azure.microsoft.com" target="_blank" title="Azure">Azure</a>  
<a href="https://docker.com" target="_blank" title="Docker">Docker</a>  
<a href="https://kubernetes.io" target="_blank" title="Kubernetes">Kubernetes</a> 

Away from keyboard I am a 🐴 horseman 🏇, it is how I refresh and recharge. 
 

## My Current Setup:

💻 [2021 Macbook Pro 14-inch](https://www.apple.com/macbook-pro-14-and-16/specs/)  
📺 [LG 32QK500-C 32-Inch Class QHD LED IPS Monitor with Radeon FreeSync](https://www.amazon.com/gp/product/B07YGZRQ98/ref=ppx_yo_dt_b_asin_title_o03_s00?ie=UTF8&psc=1)  
⌨️ [iKBC CD87 V2 Ergonomic Mechanical Keyboard with Cherry MX Clear Switch](https://www.amazon.com/dp/B08B84VPN9?psc=1&ref=ppx_yo2ov_dt_b_product_details)  
🖱️ [SteelSeries Rival 95 Optical Gaming Mouse](https://www.amazon.com/gp/product/B075LY78QD/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)  
🪵 [WOKA Dual Motor Standing Desk, 48 x 24 Inches Adjustable Height Desk](https://www.amazon.com/gp/product/B095H2PTYK/ref=ppx_yo_dt_b_asin_title_o07_s00?ie=UTF8&psc=1)  
